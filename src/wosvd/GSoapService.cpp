#include "GSoapService.hpp"

// GSoapInstance Functions
/*******************************************************************************
 * Constructor for GSoapInstance Class
 *
 * @param service_ctx Service context holding gSoap configuration.
 ******************************************************************************/
GSoapInstance::GSoapInstance(std::shared_ptr<soap> gsoap)
    : gsoap{gsoap}, DeviceBindingService_inst{gsoap.get()},
      MediaBindingService_inst{gsoap.get()}, PTZBindingService_inst{gsoap.get()}
{
}

/*******************************************************************************
 * Destructor for GSoapInstance
 ******************************************************************************/
GSoapInstance::~GSoapInstance()
{
    return;
}

/*******************************************************************************
 * Main work function for gSoap
 *
 * This function is used by ThreadWarden to manage the gSoap connection, this
 * function essentially runs like a while(1) loop until ThreadWarden tells
 * gSoap to stop runing.
 *
 * @return 1 if error, 0 if okay
 ******************************************************************************/
int GSoapInstance::work()
{
    // wait new client
    if (!soap_valid_socket(soap_accept(gsoap.get())))
    {
        if (gsoap->accept_timeout)
        {
            //arms::log<arms::LOG_INFO>("timeout");
            return 0;
        }
        soap_stream_fault(gsoap.get(), std::cerr);
        arms::throwLog("SOAP Invalid Socket");
    }

    // process service
    arms::log<arms::LOG_INFO>("Begin serve");
    if (soap_begin_serve(gsoap.get()))
    {
        arms::log<arms::LOG_INFO>("Process Service");
        soap_stream_fault(gsoap.get(), std::cerr);
    }
    FOREACH_SERVICE(DISPATCH_SERVICE, gsoap.get())
    else
    {
        arms::log<arms::LOG_INFO>("Unknown service");
        return 1;
    }

    return 0;
}



/*******************************************************************************
 * HTTP GET Callback function
 *
 * This function will be called if the gSoap instance recieves a GET request
 * over HTTP, this will initially be used to serve a snapshot image but may
 * eventually be used to help configure the deamon from a web interface
 *
 * @return HTTP Response codes (404 for error)
 ******************************************************************************/
//int GSoapInstance::http_get(struct soap *soap)
//{
//    if (strchr(soap->path + 1, '/') || strchr(soap->path + 1, '\\'))
//        return 403;
//    if (!soap_tag_cmp(soap->path, "*.html"))
//        return copy_file(soap, soap->path + 1, "text/html");
//    if (!soap_tag_cmp(soap->path, "*.xml") || !soap_tag_cmp(soap->path, "*.xsd") || !soap_tag_cmp(soap->path, "*.wsdl"))
//        return copy_file(soap, soap->path + 1, "text/xml");
//    if (!soap_tag_cmp(soap->path, "*.jpg"))
//        return copy_file(soap, soap->path + 1, "image/jpeg");
//    if (!soap_tag_cmp(soap->path, "*.gif"))
//        return copy_file(soap, soap->path + 1, "image/gif");
//    if (!soap_tag_cmp(soap->path, "*.png"))
//        return copy_file(soap, soap->path + 1, "image/png");
//    if (!soap_tag_cmp(soap->path, "*.ico"))
//        return copy_file(soap, soap->path + 1, "image/ico");
//    return 404; /* HTTP not found */
//}

/*******************************************************************************
 * Copy File
 *
 * This function will take a file from the device the daemon is running from,
 * open it up and serve it to the device over HTTO, images and http files are
 * supported at the moment
 *
 * @return SOAP Status
 ******************************************************************************/
//int GSoapInstance::copy_file(struct soap *soap, const char *name, const char *type)
//{
//    printf("NAME: %s", name);
//    FILE *fd;
//    size_t r;
//    fd = fopen(name, "rb"); /* open file to copy */
//    if (!fd)
//        return 404; /* return HTTP not found */
//    soap->http_content = type;
//    if (soap_response(soap, SOAP_FILE)) /* OK HTTP response header */
//    {
//        soap_end_send(soap);
//        fclose(fd);
//        return soap->error;
//    }
//    for (;;)
//    {
//        r = fread(soap->tmpbuf, 1, sizeof(soap->tmpbuf), fd);
//        if (!r)
//            break;
//        if (soap_send_raw(soap, soap->tmpbuf, r))
//        {
//            soap_end_send(soap);
//            fclose(fd);
//            return soap->error;
//        }
//    }
//    fclose(fd);
//    return soap_end_send(soap);
//}
