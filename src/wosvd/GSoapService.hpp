#ifndef GSOAPSERVICE_H
#define GSOAPSERVICE_H

// ---- project includes ----
#include "ServiceContext.h"

// ---- gsoap ----
#include "soapDeviceBindingService.h"
#include "soapMediaBindingService.h"
#include "soapPTZBindingService.h"

// ---- armoury ----
#include "armoury/files.hpp"
#include "armoury/logger.hpp"

#define FOREACH_SERVICE(APPLY, soap)                                                                                     \
    APPLY(DeviceBindingService, soap)                                                                                    \
    APPLY(MediaBindingService, soap)                                                                                     \
    APPLY(PTZBindingService, soap)                                                                                       \
    /*                                                                                                                   \
     * If you need support for other services,                                                                           \
     * add the desired option to the macro FOREACH_SERVICE.                                                              \
     *                                                                                                                   \
     * Note: Do not forget to add the gsoap binding class for the service,                                               \
     * and the implementation methods for it, like for DeviceBindingService                                              \
                                                                                                                       \ \
                                                                                                                       \ \
                                                                                                                       \ \
            APPLY(ImagingBindingService, soap)                                                                           \
            APPLY(PTZBindingService, soap)                                                                               \
            APPLY(RecordingBindingService, soap)                                                                         \
            APPLY(ReplayBindingService, soap)                                                                            \
            APPLY(SearchBindingService, soap)                                                                            \
            APPLY(ReceiverBindingService, soap)                                                                          \
            APPLY(DisplayBindingService, soap)                                                                           \
            APPLY(EventBindingService, soap)                                                                             \
            APPLY(PullPointSubscriptionBindingService, soap)                                                             \
            APPLY(NotificationProducerBindingService, soap)                                                              \
            APPLY(SubscriptionManagerBindingService, soap)                                                               \
    */

#define DECLARE_SERVICE(service, soap) service service##_inst(soap);

#define DISPATCH_SERVICE(service, soap)                                                                                \
    else if (service##_inst.dispatch() != SOAP_NO_METHOD)                                                              \
    {                                                                                                                  \
        soap_send_fault(soap);                                                                                         \
        soap_stream_fault(soap, std::cerr);                                                                            \
    }


/*******************************************************************************
 * GSoapInstance
 *
 * This class is used to run gSoap via ThreadWarden
 ******************************************************************************/
class GSoapInstance
{
  public:
    static constexpr char const *g_workerName{"soap"};
    static constexpr bool g_copyDataOnce{true};
    struct Input
    {
    } dataIn;
    struct Output
    {
    } dataOut;

    int work();

    GSoapInstance(std::shared_ptr<soap> gsoap);
    ~GSoapInstance();

    //static int http_get(struct soap *soap);
    //static int copy_file(struct soap *, const char *, const char *); /* copy file as HTTP response */

  private:
    std::shared_ptr<soap> gsoap;
    DeviceBindingService DeviceBindingService_inst;
    MediaBindingService MediaBindingService_inst;
    PTZBindingService PTZBindingService_inst;
};

#endif // GSOAPSERVICE_H
