#include <errno.h>
#include <getopt.h>
#include <libconfig.h++>
#include <libconfig.h>
#include <list>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>

#include <sys/socket.h> // for LINGER
#include <memory>

#include "ConfigLoader.hpp"
#include "Configuration.hpp"
#include "DeviceBinding.nsmap"
#include "GSoapService.hpp"
#include "armoury/ThreadWarden.hpp"
#include "daemon.hpp"
#include "smacros.h"

/*******************************************************************************
 * Verification function to ensure that service_ctx is holding the correct
 * information.
 ******************************************************************************/
inline void checkServiceCtx(ServiceContext const &serviceContext)
{
    if (serviceContext.eth_ifs.empty())
        throw std::runtime_error("Error: not set no one ehternet interface more details see opt --ifs\n");

    if (serviceContext.scopes.empty())
        throw std::runtime_error("Error: not set scopes more details see opt --scope\n");

    if (serviceContext.get_profiles().empty())
        throw std::runtime_error("Error: not set no one profile more details see --help\n");
}

void processing_cfg(Configuration const &configStruct, ServiceContext &service_ctx, Daemon &onvifDaemon)
{
    // New function to handle config file
    StreamProfile profile;
    DaemonInfo daemonInfo;

    // Get Daemon info
    daemonInfo.set_pidFile(configStruct.pid_file);
    daemonInfo.set_logLevel(configStruct.logLevel);
    daemonInfo.set_logFile(configStruct.logFile);
    daemonInfo.set_logFileSizeMb(configStruct.logFileSizeMb);
    daemonInfo.set_logFileCount(configStruct.logFileCount);
    daemonInfo.set_logAsync(configStruct.logAsync);



    if (!onvifDaemon.SaveConfig(daemonInfo))
        onvifDaemon.daemon_error_exit("Can't save daemon info: %s\n", service_ctx.get_cstr_err());

    // ONVIF Service Options
    service_ctx.port = configStruct.port;
    service_ctx.user = configStruct.user.c_str();
    service_ctx.password = configStruct.password.c_str();
    service_ctx.manufacturer = configStruct.manufacturer.c_str();
    service_ctx.model = configStruct.model.c_str();
    service_ctx.firmware_version = configStruct.firmware_version.c_str();
    service_ctx.serial_number = configStruct.serial_number.c_str();
    service_ctx.hardware_id = configStruct.hardware_id.c_str();

    for (auto it = begin(configStruct.scopes); it != end(configStruct.scopes); ++it)
    {
        service_ctx.scopes.push_back(it->scopeUri);
    }

    service_ctx.eth_ifs.push_back(Eth_Dev_Param());
    if (service_ctx.eth_ifs.back().open(configStruct.interfaces.c_str()) != 0)
        onvifDaemon.daemon_error_exit("Can't open ethernet interface: %s - %m\n", configStruct.interfaces.c_str());

    if (!service_ctx.set_tz_format(configStruct.tz_format.c_str()))
        onvifDaemon.daemon_error_exit("Can't set tz_format: %s\n", service_ctx.get_cstr_err());

    // Onvif Media Profiles
    for (auto it = begin(configStruct.profiles); it != end(configStruct.profiles); ++it)
    {
        profile.set_name(it->name.c_str());
        profile.set_width(it->width.c_str());
        profile.set_height(it->height.c_str());
        profile.set_url(it->url.c_str());
        profile.set_snapurl(it->snapUrl.c_str());
        profile.set_type(it->type.c_str());

        if (!service_ctx.add_profile(profile))
            onvifDaemon.daemon_error_exit("Can't add Profile: %s\n", service_ctx.get_cstr_err());

        DEBUG_MSG("configured Media Profile %s\n", profile.get_name().c_str());
        profile.clear();
    }

}

int main()
{
    arms::signals::initSignals();

    std::optional<std::string> const configFile{arms::files::findConfigFile("wosvd.conf")};
    if (configFile)
    {
        DEBUG_MSG("Found config file: %s\n", configFile.value().c_str());
    }
    else
    {
        DEBUG_MSG("Unable to find config file, using defaults.\n");
    }

    Configuration const configStruct{configFile};
    ServiceContext serviceContext{};
    Daemon onvifDaemon;

    DEBUG_MSG("processing_cfg\n");
    processing_cfg(configStruct, serviceContext, onvifDaemon);

    DEBUG_MSG("Initialising logging...");
    arms::logger::setupLogging(configStruct.logLevel, configStruct.logAsync, configStruct.logFile,
                               configStruct.logFileSizeMb, configStruct.logFileCount);
    DEBUG_MSG("...done\n");
    arms::log<arms::LOG_INFO>("Logging Enabled");

    auto gsoapDeleter = [](soap *ptr) {
        soap_destroy(ptr); // delete managed C++ objects
        soap_end(ptr);     // delete managed memory
        soap_done(ptr);
        soap_free(ptr);
    };
    std::shared_ptr<soap> gsoap{soap_new(), gsoapDeleter};
    if (!gsoap)
    {
        throw std::runtime_error("Unable to create soap instance");
    }

    gsoap->bind_flags = SO_REUSEADDR;

    if (!soap_valid_socket(soap_bind(gsoap.get(), NULL, serviceContext.port, 10)))
    {
        soap_stream_fault(gsoap.get(), std::cerr);
        exit(EXIT_FAILURE);
    }

    //gsoap->fget = http_get;

    gsoap->send_timeout = -3'000'000;
    gsoap->recv_timeout = -3'000'000;
    gsoap->connect_timeout = -5'000'000;
    gsoap->accept_timeout = -500'000;
    gsoap->accept_flags |= SO_LINGER;
    gsoap->linger_time = 60;

    // save pointer of service_ctx in soap
    gsoap->user = static_cast<void *>(&serviceContext);

    checkServiceCtx(serviceContext);

    FOREACH_SERVICE(DECLARE_SERVICE, gsoap.get())

    arms::ThreadWarden<GSoapInstance, std::shared_ptr<soap>> gsoapWorker{gsoap};
    gsoapWorker.start();

    while (!arms::signals::shouldExit())
    {
        if (gsoapWorker.checkAndRestartOnFailure())
        {
            arms::log<arms::LOG_WARNING>("gsoapWorker failed");
        }
        arms::time::microSleep(500'000);
    }

    auto forceClose = [&]()
    {
        //gsoap->fdisconnect(gsoap.get());
        //close(gsoap->master);
        soap_force_closesock(gsoap.get());
//        linger ling = {0, 0};
//        setsockopt(gsoap->socket, SOL_SOCKET, SO_LINGER,
//                   (char*)&ling, sizeof(linger));
//        if (!close(gsoap->socket))
//        {
//            arms::log<arms::LOG_WARNING>("error closing socket");
//        }
    };
    //arms::ensureStop(gsoapWorker, forceClose);

    return EXIT_SUCCESS;
    //return EXIT_FAILURE; // Error, normal exit from the main loop only through the signal handler.
}
