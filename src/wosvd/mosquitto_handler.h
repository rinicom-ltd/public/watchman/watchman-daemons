#ifndef MOSQUITTO_HANDLER
#define MOSQUITTO_HANDLER

#include <errno.h>
#include <fcntl.h>
#include <mosquitto.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>

// Macros/Constants

// Global Variables

// Function Definitions
void init_mosquitto(struct mosquitto *mosq);
int publish_to_watchman(struct mosquitto *mosq_, int payloadLen, const char *payload);
void on_publish();

#endif // MOSQUITTO_HANDLER
