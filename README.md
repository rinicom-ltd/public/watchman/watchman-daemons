## Watchman Daemons

### wosvd
Watchman ONVIF Server Daemon. Forked from [onvif_srvd](https://github.com/KoynovStas/onvif_srvd)

### wwsdd
Watchman WS-Discovery Daemon. Forked from [wsdd](https://github.com/KoynovStas/wsdd)

###

### Development dependencies

#### Fedora 36

``` dnf install clang-tools-extra gsoap-devel openssl-devel```

###

### Background

ONVIF official website: [https://www.onvif.org](https://www.onvif.org)
and their [github presence](https://github.com/onvif/).

The web services data binding is generated using [gSOAP](https://www.genivia.com)

Show how enable support WS-Security:
```console
make WSSE_ON=1
```

### Testing

For testing there are a few client applications available.

1. [ONVIFViewer](https://flathub.org/apps/details/net.meijn.onvifviewer)

2. [ONVIF Device Tool](http://lingodigit.com/onvif_nvc.html)
   (Requires support for WS-Security and WS-Discovery)

3. [gsoap-onvif](https://github.com/tonyhu/gsoap-onvif)
   (Terminal application)

4. [ONVIF Device Manager](https://sourceforge.net/projects/onvifdm/) (Windows only)



### License

[BSD-3-Clause](./LICENSE).
